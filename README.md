# The QCTOOL repository has moved #

**27th June 2020**

Due to Bitbucket's sunsetting of support for mercurial hosting,
I have moved the QCTOOL repository to new hosting at:

https://code.enkre.net/qctool

Along with the code, the majority of issues and existing wiki pages have been
moved to the new hosting.

The new hosting is an experimental approach using the [Fossil SCM](https://fossil-scm.org); please
report any issues to the mailing list at:

`OXSTATGEN@JISCMAIL.AC.UK`

or to me by email.

The QCTOOL documentation can be found at https://www.well.ox.ac.uk/~gav/qctool.

Best wishes,
Gavin.

